# Example Ansible playbook

## How to use

1. Install dependencies:

   ```
   sudo apt install ansible vagrant virtualbox
   ```

2. Clone the repository:

   ```
   git clone https://gitlab.com/mejo-/example-playbook.git
   cd example-playbook
   ```

3. Start up the vagrant box:

   ```
   vagrant up
   ```

4. Run the Ansible playbook:

   ```
   ansible-playbook site.yml --diff
   ```
